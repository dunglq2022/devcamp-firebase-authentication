import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

// TODO: Replace the following with your app's Firebase project configuration
// See: https://firebase.google.com/docs/web/learn-more#config-object
const firebaseConfig = {
    apiKey: "AIzaSyCw0-M-FJ0IJBeHGwKSzAE6Nh_wxBl5py4",
    authDomain: "shop24-4c2b3.firebaseapp.com",
    projectId: "shop24-4c2b3",
    storageBucket: "shop24-4c2b3.appspot.com",
    messagingSenderId: "361387117095",
    appId: "1:361387117095:web:85b8615a45184401ff1c02"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);

export default auth;
