import logo from './logo.svg';
import './App.css';
import auth from './firebase';
import { GoogleAuthProvider, onAuthStateChanged, signInWithPopup, signOut } from 'firebase/auth';
import { useEffect, useState } from 'react';

const provider = new GoogleAuthProvider();

function App() {
  const [user, setUser] = useState(null);
 
  const loginGoogle = () => {
    signInWithPopup(auth, provider)
    .then((result) => {
      setUser(result.user)
      console.log(result)
    })
    .catch((error) => {
      console.log('Error', error);
    })
  }

  useEffect(() => {
    onAuthStateChanged(auth, (result) => {
      if(result){
        setUser(result)
        console.log(result)
      } else {
        setUser(null)
        console.log(result)
      }
    })
  },[])

  const logoutGoolge = () => {
      signOut(auth)
      .then(() => {
        setUser(null)
      })
      .catch((error) => {
        console.log("logout Error", error)
      })
  }
  
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {user ? 
        <>
          <img src={user.photoURL} alt='anh-google' />
          <p>Hello user {user.displayName}</p>
          <button onClick={logoutGoolge} >Log out</button>
        </>
        :
        <>
          <p>
          Please Signin.
          </p>
          <button onClick={loginGoogle}>Signin with Google</button>
        </>  
        }
        
      </header>
    </div>
  );
}

export default App;
